// console.log("Hello World");

/*
	Step 3 and Step 4 - (Both Not Working)
	GET METHOD
*/

fetch("https://jsonplaceholder.typicode.com/todos")
// .then((response) => response.map(object => {
// 		return [object.title];
// }))
// .then((map) => console.log(map));

.then((response) => response.json())
.then((json) => console.log(json));

/*
	Step 5 and Step 6 (Not Working)
	GET METHOD
*/

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json));
// console.log(`the item ${response.title} on the list has a status of ${response.completed}`)

/*
	Step 7
	POST METHOD
*/

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type" :"application/json"
	},
	body: JSON.stringify({
		completed: false,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

/*
	Step 8 and Step 9
	PUT METHOD
*/

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type" :"application/json"
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update my to do list with different data structure.",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

/*
	Step 10 and Step 11
	PATCH METHOD
*/

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type" :"application/json"
	},
	body: JSON.stringify({
		dateCompleted: "10/26/2022",
		status: "Complete"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

/*
	Step 12
	DELETE METHOD
*/

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH"
});
